import Foundation
import CoreData

class CoreDataSingleton {
    typealias SaveCallback = (_ error: Error?, _ didSaveNewData: Bool) -> Void
    static let sharedInstance = CoreDataSingleton()

    private lazy var diskManagedObjectContext: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()

    lazy var uiManagedObjectContext: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        // ui object context doesnt perform saves, the disk object context does
        managedObjectContext.parent = self.diskManagedObjectContext
        return managedObjectContext
    }()

    lazy var workerManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.parent = self.uiManagedObjectContext
        return managedObjectContext
    }()

    func saveAll(_ saveCallback: @escaping SaveCallback) {
        save(context: workerManagedObjectContext) { (error, didSaveNewData) in
            guard error == nil else {
                LogError("failed to save worker context", error: error!)
                saveCallback(error, didSaveNewData)
                return
            }

            guard didSaveNewData else {
                NSLog("No new data in worker context, no point bubbling up to ui context")
                saveCallback(error, didSaveNewData)
                return
            }

            self.save(context: self.uiManagedObjectContext, callback: { (error, didSaveNewData) in
                guard error == nil else {
                    LogError("failed to save ui context", error: error!)
                    saveCallback(error, didSaveNewData)
                    return
                }

                guard didSaveNewData else {
                    NSLog("No new data in ui context, no point bubbling up to disk context")
                    saveCallback(error, didSaveNewData)
                    return
                }

                self.save(context: self.diskManagedObjectContext, callback: saveCallback)
            })
        }
    }

    func deleteAllObjects(_ saveCallback: @escaping SaveCallback) {
        workerManagedObjectContext.perform {
            // FIXME: Really we should ask JsonToDatabase to perform these deletes rather than
            // just asking it for the list
            for entityName in JsonToDatabase.managedObjectEntitiesToClearOnLogout() {
                do {
                    try self.deleteAllObjects(forEntity: entityName, context: self.workerManagedObjectContext)
                } catch {
                    LogError("Failed to delete all objects", error: error)
                }
            }
            self.saveAll(saveCallback)
        }
    }

    // only run on the worker thread
    private func deleteAllObjects(forEntity entityName: String, context: NSManagedObjectContext) throws {
        let request = NSFetchRequest<NSManagedObject>()
        request.entity = NSEntityDescription.entity(forEntityName: entityName, in: context)

        let objects = try context.fetch(request)
        for object in objects {
            context.delete(object)
        }
    }

    private func save(context: NSManagedObjectContext, callback: @escaping SaveCallback) {
        context.perform {
            guard context.hasChanges else {
                self.callbackOnMainThread(callback, error: nil, didSaveNewData: false)
                return
            }

            do {
                try context.save()
                self.callbackOnMainThread(callback, error: nil, didSaveNewData: true)
            } catch {
                LogError("Failed to save context", error: error)
                self.callbackOnMainThread(callback, error: error, didSaveNewData: false)
            }
        }
    }

    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let fileManager = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).last!
        let storeURL = documentsDirectory.appendingPathComponent("Gitter.sqlite")

        let modelURL = Bundle.main.url(forResource: "Gitter", withExtension: "momd")!
        let model = NSManagedObjectModel(contentsOf: modelURL)!

        if (fileManager.fileExists(atPath: storeURL.path) && !isStoreCompatible(storeURL, withModel: model)) {
            NSLog("Core data store is incompatible with model. Deleting store.")
            do {
                try fileManager.removeItem(at: storeURL)
            } catch {
                LogError("Failed to delete incompatible store, carrying on anyway.", error: error)
            }
        }

        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            LogError("Failed to initialize Gitter core data", error: error)
            abort()
        }

        return coordinator
    }()

    private func callbackOnMainThread(_ callback: @escaping SaveCallback, error: Error?, didSaveNewData: Bool) {
        DispatchQueue.main.async {
            callback(error, didSaveNewData)
        }
    }
}

private func isStoreCompatible(_ storeURL: URL, withModel model: NSManagedObjectModel) -> Bool {
    var isCompatible = false
    do {
        let storeMetadata = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeURL, options: nil)
        isCompatible = model.isConfiguration(withName: nil, compatibleWithStoreMetadata: storeMetadata)
    } catch {
        LogError("Failed to check if core data store is compatible, assuming incompatible", error: error)
    }

    return isCompatible
}
